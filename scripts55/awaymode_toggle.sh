#!/usr/bin/env sh

# Toggle on/off an "away mode".
# This mode is used in the notification hook
# To send notifications to a matrix room if
# in away mode, or just vibrate as normal 
# if not.

# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "$(which sxmo_common.sh)"

AWAY_FILE="$XDG_CONFIG_HOME/sxmo/away_mode"

# Create file if it doesn't exist yet.
if [ ! -f "$AWAY_FILE" ]; then
	echo "0" > "$AWAY_FILE"
fi

enable_away_mode() {
	echo "1" > "$AWAY_FILE"
}

disable_away_mode() {
	echo "0" > "$AWAY_FILE"
}

AWAY_STATUS="$(cat "$AWAY_FILE")"
if [ "$AWAY_STATUS" = "1" ]; then
	disable_away_mode
else
	enable_away_mode
fi
